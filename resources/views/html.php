{{-- Pesan Sukses --}}
@if (session()->has('success'))
<div id="notifikasi">
    <div class="alert alert-success">
        <p> {{ session('success') }} !</p>
    </div>
</div>
@endif
{{-- Pesan Sukses --}}

{{-- Pesan Gagal --}}
@if (session()->has('destroy'))
<div id="notifikasi">
    <div class="alert alert-danger">
        <p> {{ session('destroy') }} !</p>
    </div>
</div>
@endif
{{-- Pesan Gagal --}}

<a href="/cast/create" class="btn btn-primary">Tambah</a><br>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Trident</td>
            <td>Internet Explorer 4.0</td>
            <td>Win 95+</td>
            <td>4</td>
            <td>X</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>
    </tfoot>
</table>