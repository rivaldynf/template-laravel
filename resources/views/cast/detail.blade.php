@extends('templates.master')

@section('title')
Halaman Data Table
@endsection

@section('content')

<h2>Show Cast {{ $cast->id }}</h2>
<h4>{{ $cast->nama }}</h4>
<h4>{{ $cast->umur }}</h4>
<p>{{ $cast->bio }}</p>
<a href="/cast" class="btn btn-danger btn-md">Kembali</a>

@endsection

