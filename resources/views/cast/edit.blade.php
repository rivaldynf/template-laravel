@extends('templates.master')

@section('title')
Halaman Data Table
@endsection

@section('content')

<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="nama">Nama</label>
        <input
            type="text"
            class="form-control"
            name="nama"
            id="nama"
            value="{{ $cast->nama }}"
            placeholder="Masukkan Nama"
        />
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="umur">Umur</label>
        <input
            type="text"
            class="form-control"
            name="umur"
            id="umur"
            value="{{ $cast->umur }}"
            placeholder="Masukkan Umur"
        />
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="bio">bio</label>
        <input
            type="text"
            class="form-control"
            name="bio"
            id="bio"
            value="{{ $cast->bio }}"
            placeholder="Masukkan Bio"
        />
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
    <a href="/cast" class="btn btn-danger btn-md">Kembali</a>

</form>



@endsection

