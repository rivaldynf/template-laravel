@extends('templates.master')

@section('title')
Halaman Data Table
@endsection

@section('content')

{{-- Pesan Sukses --}}
@if (session()->has('success'))
<div id="notifikasi">
    <div class="alert alert-success">
        <p> {{ session('success') }} !</p>
    </div>
</div>
@endif
{{-- Pesan Sukses --}}

{{-- Pesan Gagal --}}
@if (session()->has('destroy'))
<div id="notifikasi">
    <div class="alert alert-danger">
        <p> {{ session('destroy') }} !</p>
    </div>
</div>
@endif
{{-- Pesan Gagal --}}

<a href="/cast/create" class="btn btn-primary">Tambah</a><br>

<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cast as $value) 
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $value->nama }}</td>
            <td>{{ $value->umur }}</td>
            <td>{{ $value->bio }}</td>
            <td>
                <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection

@push('script')
  {{-- tambahan datatables --}}
  <script src="{{ asset('/backend_asset/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('/backend_asset/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
  {{-- tambahan datatables --}}
@endpush
